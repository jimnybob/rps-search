package service

import org.junit.{Assert, Test}

/**
 * Created by James on 04/04/2014.
 */
class QueryBuilderTest {

  @Test
  def query() = {

    val qBuilder: QueryBuilder = new QueryBuilder()
    qBuilder.addLeftHandDrug("alcoh")
    qBuilder.addRightHandDrug("fluor")
    qBuilder.addRightHandDrug("hyo")

    val expected = "MATCH (lHand:Interactant)<-[]->(intr)<-[]->(rHand:Interactant) WHERE lHand.name =~ '(?i)alcoh.*' AND (rHand.name =~ '(?i)hyo.*' OR rHand.name =~ '(?i)fluor.*') AND lHand <> rHand RETURN DISTINCT lHand.name as lname, intr.identity_number as idnum, rHand.name as rname LIMIT 25"
    Assert.assertEquals(expected, qBuilder.build())

  }
}
