package service

import scala.collection.JavaConversions._

import org.junit.{Assert, Test}
import controllers.QueryController.Query
import org.neo4j.rest.graphdb.batch.CypherResult
import java.util.Map.Entry
import org.anormcypher.CypherStatement
import service.Neo4jService.Row


/**
 * Created by James on 03/04/2014.
 */
class Neo4jServiceTest {

  @Test
  def find() = {

    val rows: List[Row] = Neo4jService.findData(Query(Some("Alcohol"), None))
    println(rows.mkString("\n"))
    Assert.assertEquals(4, rows.size)
  }

}
