package service

/**
 * Created by James on 04/04/2014.
 */
class QueryBuilder {

  var leftHandDrug: String = ""
  var drugNames: List[String] = List[String]()

  def addLeftHandDrug(name: String) = {
    leftHandDrug = name;
    this
  }

  def addRightHandDrug(name: String) = {
    drugNames ::= name
    this
  }

  def addRightHandDrugs(name: Option[List[String]]): QueryBuilder = name match {
    case dn: Some[List[String]] => drugNames :::= dn.get; this
    case _ => this
  }

  def build(): String = {

    var query: String = "MATCH (lHand:Interactant)<-[]->(intr)<-[]->(rHand:Interactant) " +
      "WHERE lHand.name =~ '(?i)" + leftHandDrug + ".*' AND "
    if (drugNames.nonEmpty) {
      query += drugNames.mkString("(rHand.name =~ '(?i)", ".*' OR rHand.name =~ '(?i)", ".*')")
      query += " AND "
    }
    query += "lHand <> rHand " +
      "RETURN DISTINCT lHand.name as lname, intr.identity_number as idnum, rHand.name as rname LIMIT 25"

    query
  }
}
