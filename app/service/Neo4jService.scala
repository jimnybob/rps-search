package service

import scala.collection.JavaConversions._
import controllers.QueryController.Query
import org.anormcypher._

/**
 * Created by James on 03/04/2014.
 */
object Neo4jService {

  case class Row(lDrugName: String, intrId: String, rDrugName: String)

  def findData(userQuery: Query): List[Row] = {

    Neo4jREST.setServer("localhost", 7474, "/db/data/")

    val queryBuilder: QueryBuilder = new QueryBuilder
    queryBuilder.addLeftHandDrug(userQuery.drugName.get)
    queryBuilder.addRightHandDrugs(userQuery.rDrugNames)
    Cypher(queryBuilder.build()).apply().map(row =>
      Row(row[String]("lname"), row[String]("idnum"), row[String]("rname"))
    ).toList
  }
}
