$(function() {
    // add a click handler to the button
    $("#getMessageButton").click(function(event) {
        // make an ajax get request to get the message
        jsRoutes.controllers.QueryController.query().ajax({
            data: jQuery( '#search-form' ).serialize(),
            success: function(data) {
                console.log(data);
                $.each(data, function(i, item) {
                   $('#resultsTable').append('<tr class="'+ oddRow(i) +'"><td>' + item.lDrugName+'</td><td>' + item.intrId+'</td><td>' + item.rDrugName+'</td></tr>');
                });
            },
            error: function( data ){
                console.log( data.responseText );
            }
        })
    })

    rDrugInputCount = 1;
    $("#addDrug").click(function(event) {
        jQuery('#rDrugName').after('<div><input name="rDrugNames['+ (rDrugInputCount++) +']" type="text" /><button type="button" onclick="$(this).closest(\'div\').remove();">-</button></div>')
    })

    $("#clearResultsBtn").click(function(event) {
        $('#resultsTable').find("tr:gt(0)").remove();
    })

    function oddRow(i) {
        return i % 2 == 0 ? "oddRow" : "";
    }
})