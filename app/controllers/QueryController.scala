package controllers

import play.api.mvc.{Action, Controller}
import play.api.libs.json.Json
import play.api.Routes
import play.api.data._
import play.api.data.Forms._
import service.Neo4jService.Row
import service.Neo4jService

case class Message(value: String)

object QueryController extends Controller {

  implicit val msgWrites = Json.writes[Message]
  implicit val rowWrites = Json.writes[Row]

  case class Query(drugName: Option[String], rDrugNames: Option[List[String]])
  
  val userForm = Form(
    mapping(
      "drugName" -> optional(text),
      "rDrugNames" -> optional(list(text))
    )(Query.apply)(Query.unapply)
  )

  def query = Action {implicit request =>
    userForm.bindFromRequest().fold(
      formWithErrors => {
        BadRequest( formWithErrors.errors.mkString("<br />") )
      },
      user => {
        //validation 2 goes here
        Ok(Json.toJson(Neo4jService.findData(Query(user.drugName, user.rDrugNames))) )
      }
    )
  }

  def javascriptRoutes = Action { implicit request =>
    Ok(Routes.javascriptRouter("jsRoutes")(routes.javascript.QueryController.query)).as(JAVASCRIPT)
  }

}