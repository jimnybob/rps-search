import sbt.Keys._
import sbt.Keys._

name := """hello-play"""

version := "1.0-SNAPSHOT"

resolvers ++= Seq(
  "anormcypher" at "http://repo.anormcypher.org/",
  "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/"
)

resolvers +=
  "mvn-neo4j" at "http://m2.neo4j.org/content/groups/everything"

libraryDependencies ++= Seq(
  // Select Play modules
  //jdbc,      // The JDBC connection pool and the play.api.db API
  //anorm,     // Scala RDBMS Library
  //javaJdbc,  // Java database API
  //javaEbean, // Java Ebean plugin
  //javaJpa,   // Java JPA plugin
  //filters,   // A set of built-in filters
  javaCore,  // The core Java API
  // WebJars pull in client-side web libraries
  "org.webjars" %% "webjars-play" % "2.2.0",
  "org.webjars" % "bootstrap" % "2.3.1",
  // Add your own project dependencies in the form:
  // "group" % "artifact" % "version"
  "junit" % "junit" % "4.8.1" % "test",
  "org.anormcypher" %% "anormcypher" % "0.4.4"
)

libraryDependencies += "org.neo4j" % "neo4j" % "2.1.0-M01" classifier "docs"

libraryDependencies += "org.neo4j" % "neo4j-rest-graphdb" % "2.0.1"

libraryDependencies += "com.sun.jersey" % "jersey-core" % "1.9"

play.Project.playScalaSettings
